const slideshow = document.querySelector(".slideshow"),
    slide = slideshow.querySelectorAll(".slide"),
    dots = slideshow.querySelectorAll(".dot");

let slideIndex = 0;

function handleBtn(input) {
    slideIndex = slideIndex + input;
    showSlide(slideIndex);
}

function handleDot(input) {
    showSlide(input);
}

function checkNum (inNum) {
    if (inNum >= slide.length) {
        inNum = 0;
    } else if (inNum < 0) {
        inNum = slide.length - 1;
    }
    return inNum;
}

function showSlide(index) {
    slideIndex = checkNum(index);
    var i;
    for (i = 0; i < slide.length; i++) {
        slide[i].style.display = "none";
    }
    slide[slideIndex].style.display = "block";

    for (i = 0; i < slide.length; i++) {
        dots[i].className = dots[i].className.replace(" active", "");
    }
    dots[slideIndex].className += " active";
}

function init() {
    showSlide(slideIndex);
}

init();

